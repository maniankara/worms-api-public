#!/usr/bin/env bash

# Start matoseuranta api service
function main() {
  mato_home=`dirname $0`;
  cd $mato_home;
  cp /var/tmp/sails.out /var/tmp/sails.`date +%Y-%m-%d-%H-%M-%S`.out;
  export PATH=$PATH:/opt/node/node-v4.4.7-linux-x64/bin/;
  /home/matot/node_modules/.bin/sails lift --models.migrate=alter --port 3080 &>/var/tmp/sails.out

}

main $*
