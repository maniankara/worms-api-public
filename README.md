# wormsAPI

a [Sails](http://sailsjs.org) application

## Installation ##
At the moment, there is support only for Debain/RedHat based systems

### Software requirements ###
The following should be installed before setting up the api

| Installables  | Debian based                    | Redhat based                |
| ------------- | ------------------------------  | --------------------------- |
|  git          | `sudo apt-get install -y git`   | `sudo yum install -y git`   |
|  mysql/mariadb| `sudo apt-get install -y mysql-server`| `sudo yum install -y mariadb-server`|
|  nodejs       | `sudo apt-get install -y nodejs`| `sudo yum install -y nodejs`|
|  sailsjs      | `sudo npm install -g sails sails-mysql mysql`| `sudo yum install -y nodejs`|