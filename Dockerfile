FROM artificial/docker-sails

ADD . /server

RUN cd /server && \
  npm install 

CMD ["sails", "lift" ,"--models.migrate='drop'"]
