/**
 * SampleController
 *
 * @description :: Server-side logic for managing samples
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var Promise = require('bluebird');
var fs = Promise.promisifyAll(require('fs'));
var util = require('util');
var json2csv = Promise.promisify(require('json2csv'));
var _ = require('lodash');

var _cleanUpFiles = function (uploadedFiles) {
  uploadedFiles.map(function (val) {
    return fs.unlinkAsync(val['fd']);
  });

  return Promise.all(uploadedFiles);
};


var _standardFind = function (req) {

  var where = _.defaults({publicState: ['public', 'partial']}, actionUtil.parseCriteria(req))

  var query = Sample.find()
  .where(where)
  // .limit(actionUtil.parseLimit(req)) //THIS DEFAULTS REALLY LOW, SO DISABLE FOR NOW
  .skip(actionUtil.parseSkip(req))
  .sort(actionUtil.parseSort(req));

  return actionUtil.populateRequest(query, req);
};

module.exports = {
  create: function (req, res, next) {
    var sighting = false;
    var sample = false;
    var sampleData = _.cloneDeep(req.body);
    var sightingData = sampleData.sighting;
    delete sampleData.sighting;

    // ios fileTransfer hack @TODO
    if (sampleData.overrideGPS == "0") sampleData.overrideGPS = false;
    if (sampleData.overrideGPS == "1") sampleData.overrideGPS = true;

    if (sampleData.lakeOrStream == "0") sampleData.lakeOrStream = false;
    if (sampleData.lakeOrStream == "1") sampleData.lakeOrStream = true;

    if (sampleData.roads == "0") sampleData.roads = false;
    if (sampleData.roads == "1") sampleData.roads = true;

    if (sampleData.buildings == "0") sampleData.buildings = false;
    if (sampleData.buildings == "1") sampleData.buildings = true;

    if (sampleData.fishing == "0") sampleData.fishing = false;
    if (sampleData.fishing == "1") sampleData.fishing = true;

    if (sampleData.paths == "0") sampleData.paths = false;
    if (sampleData.paths == "1") sampleData.paths = true;

    if (sampleData.grazing == "0") sampleData.grazing = false;
    if (sampleData.grazing == "1") sampleData.grazing = true;

    if (sampleData.crops == "0") sampleData.crops = false;
    if (sampleData.crops == "1") sampleData.crops = true;

    if (sightingData && sightingData.clitellumAbsent && sightingData.clitellumAbsent == "0") sightingData.clitellumAbsent = false;
    if (sightingData && sightingData.clitellumAbsent && sightingData.clitellumAbsent == "1") sightingData.clitellumAbsent = true;

    if (sightingData && sightingData.tailFlattens && sightingData.tailFlattens == "0") sightingData.tailFlattens = false;
    if (sightingData && sightingData.tailFlattens && sightingData.tailFlattens == "1") sightingData.tailFlattens = true;

    if (sightingData && sightingData.colourGradient && sightingData.colourGradient == "0") sightingData.colourGradient = false;
    if (sightingData && sightingData.colourGradient && sightingData.colourGradient == "1") sightingData.colourGradient = true;

    if (sightingData && sightingData.diamGT2mm && sightingData.diamGT2mm == "0") sightingData.diamGT2mm = false;
    if (sightingData && sightingData.diamGT2mm && sightingData.diamGT2mm == "1") sightingData.diamGT2mm = true;

    req.file('sighting[image]').upload(
      {
        maxBytes: 10*1024*1024, //10 MB.
        dirname: '../../uploads' //Sails default uses the ./.tmp/uploads so need to come up twice from that to get ./
      },
      function (err, uploadedFiles) {
        if (err) {
          if (err.code = 'E_EXCEEDS_UPLOAD_LIMIT') {
            err.status = 400; //400 error instead of 500 error.
          }
          return res.negotiate(err);
        }

        if (uploadedFiles.length > 1) {
          //remove clean up files because strays are bad
          _cleanUpFiles(uploadedFiles).then(function (results) {
            return res.badRequest({message: 'Too many files.'});
          });
        }
        else if (uploadedFiles.length === 1) {
          if (!sightingData) {
            sightingData = {};
          }

          sightingData.image = util.format('/uploads/%s', uploadedFiles[0]['fd'].split('/').pop());
        }

        Sample.create(sampleData).then(function (newSample) {
          sample = newSample;
          if (sightingData) {
            sightingData.sample = sample.id;
            return Sighting.create(sightingData);
          }

          return false;
        }).then(function (newSighting) {
          if (newSighting) {
            sighting = newSighting;
            return Sample.update(sample.id,{sighting: sighting.id});
          }

          return [sample];
        }).then(function (samples) {
          sample = samples.pop();
          if (sighting) {
            sample.sighting = sighting; //assign sighting by hand to save a .find().populate() since update().populate() doesn't work
          }

          // Send JSONP-friendly response if it's supported
          // (HTTP 201: Created)
          res.status(201);
          return res.ok(sample.toJSON());
        }).catch(function (err) {
          var jobs = [
            _cleanUpFiles(uploadedFiles) //remove stray image files, cause strays are bad mmkay
          ];

          if (sample) {
            jobs.push(Sample.destroy({id: sample.id})); //remove stray samples
          }

          if (sighting) {
            jobs.push(Sighting.destroy({id: sighting.id})); //remove stray sightings
          }

          return Promise.all(jobs).then(function () {
            return res.negotiate(err);
          })
        });
      }
    )
  },
  output_kml: function (req, res, next) {
    var locals = {
      layout: null
    };

    _standardFind(req).exec(function (err, samples) {
      if (err) return res.negotiate(err);

      locals.samples = samples;
      res.setHeader('Content-Type', 'application/vnd.google-earth.kml+xml');
      return res.view('sample/kml', locals);
    });
  },
  output_csv: function (req, res, next) {
    _standardFind(req).exec(function (err, samples) {
      if (err) return res.negotiate(err);

      samples = samples.map(function (sample) {
        if (sample.sighting && sample.publicState != 'public') {
          delete sample.sighting.notes;
        }

        return sample;
      });

      json2csv({
        data: samples,
        fields: [
          'habitat',
          'latitude',
          'longitude',
          'lakeOrStream',
          'paths',
          'roads',
          'buildings',
          'fishing',
          'grazing',
          'crops',
          'groundMoisture',
          'sighting.wormCount',
          'adultLitterDwellingHandExtraction',
          'adultLitterDwellingMustardExtraction',
          'juvinelleLitterDwellingHandExtraction',
          'juvinelleLitterDwellingMustardExtraction',
          'adultSoilDwellingHandExtraction',
          'adultSoilDwellingMustardExtraction',
          'juvinelleSoilDwellingHandExtraction',
          'juvinelleSoilDwellingMustardExtraction',
          'adultDeepBurrowingHandExtraction',
          'adultDeepBurrowingMustardExtraction',
          'juvinelleDeepBurrowingHandExtraction',
          'juvinelleDeepBurrowingMustardExtraction',
          'sighting.notes',
          'groupId',
          'scientist',
          'scientistOther',
          'sampledAt',
          'sighting.image'
        ]
      }).then(function (csv) {
        res.setHeader('Content-disposition', 'attachment; filename=sample.csv');
        res.setHeader('Content-Type', 'text/csv');

        return res.end(csv);
      }).catch(res.negotiate);
    });
  }
};

