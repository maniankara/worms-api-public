var util = require('util');
var Promise = require('bluebird');
var nodemailer = require('nodemailer');
var smtpPool = require('nodemailer-smtp-pool');
var config = sails.config.email;

var transporter = nodemailer.createTransport(smtpPool(config));

module.exports = {
  send: function (opts) {
    if (!opts) {
      opts = {};
    }

    opts.from = util.format('Worms <%s>', config.auth.user);

    return new Promise(function (resolve, reject) {
      return transporter.sendMail(opts, function (err, res) {
        if (err) return reject(err);
        return resolve(res);
      });
    });
  }
};