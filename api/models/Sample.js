/**
* Sample.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  attributes: {
    latitude: {
      type: 'float',
      required: true,
      max: 90,
      min: -90
    },
    longitude: {
      type: 'float',
      required: true,
      max: 180,
      min: -180
    },
    overrideGPS: {
      type: 'boolean',
      required: true
    },
    habitat: {
      type: 'string',
      enum: [
        'lehtimetsä',
        'havumetsä',
        'sekametsä',
        'maatalousmaa',
        'niityt ja kedot',
        'pensaikko',
        'nurmikko',
        'kasvimaa/kukkapenkki',
        'joutomaa'
      ],
      required: true
    },
    paths: {
      type: 'boolean',
      required: true
    },
    roads: {
      type: 'boolean',
      required: true
    },
    buildings: {
      type: 'boolean',
      required: true
    },
    fishing: {
      type: 'boolean',
      required: true
    },
    grazing: {
      type: 'boolean',
      required: true
    },
    crops: {
      type: 'boolean',
      required: true
    },
    litterLayerUnits: {
      type: 'string',
      enum: [
        'cm',
        'mm',
        'in'
      ],
      required: true,
      defaultsTo: 'cm'
    },
    lakeOrStream: {
      type: 'boolean',
      required: true
    },
    sampledAt: {
      type: 'datetime',
      required: true
    },
    groundMoisture: {
      type: 'string',
      enum: [
        'unknown',
        'kuiva',
        'kostea',
        'märkä'
      ],
      required: true
    },
    middenCount: {
      type: 'integer',
      min: 0
    },
    adultLitterDwellingHandExtraction: {
      type: 'integer'
      // required: true
    },
    adultLitterDwellingMustardExtraction: {
      type: 'integer'
      // required: true
    },
    juvinelleLitterDwellingHandExtraction: {
      type: 'integer'
      // required: true
    },
    juvinelleLitterDwellingMustardExtraction: {
      type: 'integer'
      // required: true
    },
    adultSoilDwellingHandExtraction: {
      type: 'integer'
      // required: true
    },
    adultSoilDwellingMustardExtraction: {
      type: 'integer'
      // required: true
    },
    juvinelleSoilDwellingHandExtraction: {
      type: 'integer'
      // required: true
    },
    juvinelleSoilDwellingMustardExtraction: {
      type: 'integer'
      // required: true
    },
    adultDeepBurrowingHandExtraction: {
      type: 'integer'
      // required: true
    },
    adultDeepBurrowingMustardExtraction: {
      type: 'integer'
      // required: true
    },
    juvinelleDeepBurrowingHandExtraction: {
      type: 'integer'
      // required: true
    },
    juvinelleDeepBurrowingMustardExtraction: {
      type: 'integer'
      // required: true
    },
    scientist: {
      type: 'string',
      enum: [
        'ala-asteen oppilas',
        'ylä-asteen oppilas',
        'lukiolainen',
        'korkeakouluopiskelija',
        'opettaja',
        'biologi',
        'perhe',
        'muu kansalainen'
      ],
      required: true
    },
    scientistAge: {
      type: 'integer',
      min: 0,
      max: 123
    },
    scientistOther: {
      type: 'string'
    },
    groupId: {
      type: 'string'
    },
    publicState: {
      type: 'string',
      enum: ['hidden', 'partial', 'public'],
      required: true,
      defaultsTo: 'partial'
    },
    appVersion: {
      type: 'string',
      required: true
    },
    sighting: {
      model: 'sighting'
    }
  },
  beforeValidate: function (values, cb) {
    //the app isn't sending the right thing. Need a quick hack fix for no standardPlay/worms
    if (values.standardPlot == 'No') {
      values.standardPlot = 0;
    }
    if (values.foundWorms == 'No') {
      values.foundWorms = 0;
    }


    if (values.habitat) {
      values.habitat = values.habitat.toLowerCase();
    }

    if (values.groundMoisture) {
      values.groundMoisture = values.groundMoisture.toLowerCase();
    }

    if (values.samplingMethod) {
      values.samplingMethod = values.samplingMethod.toLowerCase();
    }

    if (values.scientist) {
      values.scientist = values.scientist.toLowerCase();
    }

    if (values.publicState) {
      values.publicState = values.publicState.toLowerCase();
    }

    cb();
  }
};

