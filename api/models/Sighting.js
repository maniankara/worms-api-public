/**
* Sighting.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  attributes: {
    wormCount: {
      type: 'integer',
      required: false
    },
    classification: {
      type: 'string',
      enum: [
        'deep burrowing (anecic)',
        'soil dwelling (endogeic)',
        'litter dwelling (epigeic)',
        'juvenile',
        'unsure'
      ],
      required: false
    },
    length: {
      type: 'string',
      enum: [
        'less than 7 cm',
        '7 cm - 12 cm',
        'greater than 12 cm'
      ],
      required: false
    },
    colour: {
      type: 'string',
      enum: [
        'dark red/purple',
        'dark greenish',
        'white',
        'grey',
        'pink'
      ],
      required: false
    },
    colourGradient: {
      type: 'boolean',
      required: false
    },
    clitellumAbsent: {
      type: 'boolean',
      required: false
    },
    diamGT2m: {
      type: 'boolean'
    },
    tailFlattens: {
      type: 'boolean'
    },
    notes: {
      type: 'string'
    },
    image: {
      type: 'string'
    },
    sample: {
      model: 'sample'
    }
  },
  beforeValidate: function (values, cb) {
    if (values.classification) {
      values.classification = values.classification.toLowerCase();
    }

    if (values.length) {
      values.length = values.length.toLowerCase();
    }

    if (values.colour) {
      values.colour = values.colour.toLowerCase();
    }
    cb();
  }
};

