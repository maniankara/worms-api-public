/**
 * Group.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
var Promises = require('bluebird');
var util = require('util');

module.exports = {
  attributes: {
    name: {
      type: 'string',
      required: true
    },
    position: {
      type: 'string',
      required: true
    },
    organization: {
      type: 'string',
      required: true
    },
    city: {
      type: 'string',
      required: true
    },
    province: {
      type: 'string',
      required: true,
      maxLength: 2,
      minLength: 2
    },
    email: {
      type: 'string',
      required: true,
      email: true
    },
    send_updates: {
      type: 'boolean',
      required: true,
      defaultsTo: false
    },
    group_id: {
      type: 'string',
      required: true,
      unique: true,
      alphanumeric: true
    },
    status: {
      type: 'string',
      enum: ['pending', 'approved', 'denied'],
      required: true,
      defaultsTo: 'pending'
    }
  },
  beforeValidate: function (values, cb) {
    if (typeof values.province === 'string') {
      values.province = values.province.toUpperCase();
    }

    if (typeof values.state === 'string') {
      values.state = values.state.toLowerCase();
    }

    return cb();
  },
  afterCreate: function (record, cb) {
    //render sails templates this way:
    //sails.hooks.views.render('emails/receipt', {user: user}, doStuffCallback());
    Promises.all([
      sails.hooks.views.render('email/group_created_user', {group: record}, function (err, html) {
        if (err) throw err;

        return Email.send({
          to: util.format('%s <%s>', record.name, record.email),
          subject: 'Alberta Worm Invasion Group ID',
          html: html
        })
      }),
      sails.hooks.views.render('email/group_created_admin', {group: record}, function (err, html) {
        if (err) throw err;

        return Email.send({
          to: util.format('Worms Admin <%s>', sails.config.email.auth.user),
          subject: 'Alberta Worm Invasion Signup',
          html: html
        })
      })
    ]).finally(cb);
  },
  beforeUpdate: function (updates, cb) {
    //do a findOne and compare to updates
    if (updates['x-send-status-update-email']) {
      delete updates['x-send-status-update-email'];
      return this.findOne(updates.id).then(function (current) {
        current.status = updates.status;
        return sails.hooks.views.render('email/group_updated_user', {group: current}, function (err, html) {
          if (err) throw err;

          return Email.send({
            to: util.format('%s <%s>', current.name, current.email),
            subject: util.format('%s: Alberta Worm Invasion Group ID', updates.status.toUpperCase()),
            html: html
          });
        })

      }).finally(cb);
    }

    return cb();
  }
};

